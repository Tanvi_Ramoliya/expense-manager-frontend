import React, { Component } from "react";

class AccountList extends Component {
  state = {
    jwttoken: localStorage.getItem("jwttoken"),
    accounts: [],
    transactions: [],
    DeleteMessage: "",
    showAccountForm: false,
    AccountType: "add",
    accountName: "",
    accountId: "",
    showTransaction: false,
    showTransactionForm: false,
    transactionType: "add",
    amount : 0,
    type : '',
    TransactionId : '',
    accountIdForT : ''
  };
  logout = () => {
    localStorage.setItem("jwttoken", "");
    window.location.href = "/login";
  };
  componentDidMount() {
    fetch("http://localhost:3001/account", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + this.state.jwttoken,
      },
    })
      .then((data) => {
        return data.json();
      })
      .then((result) => {
        this.setState({ accounts: result.accounts });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  DeleteAccount = (id) => {
    fetch("http://localhost:3001/account/" + id, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + this.state.jwttoken,
      },
    })
      .then((data) => {
        return data.json();
      })
      .then((result) => {
        this.setState({ DeleteMessage: result.message });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  DeleteTransaction = (id) => {
    fetch("http://localhost:3001/transaction/" + id, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + this.state.jwttoken,
      },
    })
      .then((data) => {
        return data.json();
      })
      .then((result) => {
        this.setState({ DeleteMessage: result.message });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  addAccount = () => {
    this.setState({ showAccountForm: true, AccountType: "add" });
  };
  EditAccount = (accountInfo) => {
    this.setState({
      showAccountForm: true,
      AccountType: "edit",
      accountName: accountInfo.name,
      accountId: accountInfo._id,
    });
  };
  setValue = (e) => {
    const name = e.target.id;
    const value = e.target.value;
    this.setState({ [name]: value });
  };
  createAccount = (event) => {
    event.preventDefault();
    const { name, jwttoken, AccountType, accountId } = this.state;
    if (AccountType === "edit") {
      fetch("http://localhost:3001/account/" + accountId, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + jwttoken,
        },
        redirect: "follow",
        body: JSON.stringify({ name: name }),
      })
        .then((result) => {
          this.setState({ status: result.status });
          return result.json();
        })
        .then((r) => {
          this.setState({ successMessage: r.message, showAccountForm: false });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      fetch("http://localhost:3001/account", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + jwttoken,
        },
        redirect: "follow",
        body: JSON.stringify({ name: name }),
      })
        .then((result) => {
          this.setState({ status: result.status });
          return result.json();
        })
        .then((r) => {
          this.setState({ successMessage: r.message, showAccountForm: false });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  createTransaction = (event) => {
    event.preventDefault();
    const { type,amount, jwttoken, transactionType, TransactionId,accountIdForT } = this.state;
    console.log(type,amount,transactionType,accountIdForT)
    if (transactionType === "edit") {
      fetch("http://localhost:3001/transaction/" + TransactionId, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + jwttoken,
        },
        redirect: "follow",
        body: JSON.stringify({ type: type, amount : amount }),
      })
        .then((result) => {
          this.setState({ status: result.status });
          return result.json();
        })
        .then((r) => {
          this.setState({ successMessage: r.message, showTransactionForm: false });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      fetch("http://localhost:3001/transaction/" + accountIdForT, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + jwttoken,
        },
        redirect: "follow",
        body: JSON.stringify({ type: type,amount : amount }),
      })
        .then((result) => {
          this.setState({ status: result.status });
          return result.json();
        })
        .then((r) => {
          this.setState({ successMessage: r.message, showTransactionForm: false,amount:'',type :'' });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  getTransaction = (id) => {
    const { jwttoken } = this.state;
    this.setState({ showTransaction: true });
    fetch("http://localhost:3001/transaction/" + id, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + jwttoken,
      },
    })
      .then((result) => {
        this.setState({ status: result.status });
        return result.json();
      })
      .then((r) => {
        console.log(r);
        this.setState({ transactions: r.transaction });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  AddTransaction = (id) => {
    this.setState({ showTransactionForm: true, transactionType: "add",accountIdForT : id});
  };
  EditTransaction = (TransactionInfo) => {
      this.setState({
          showTransactionForm : true,
          transactionType : "edit",
          type : TransactionInfo.type,
          amount : TransactionInfo.amount,
          TransactionId : TransactionInfo._id
      })
  }
  render() {
    const {
      accounts,
      DeleteMessage,
      showAccountForm,
      AccountType,
      accountName,
      transactions,
      showTransaction,
      showTransactionForm,
      transactionType,
      amount
    } = this.state;
    if (this.state.jwttoken === "") {
      window.location.href = "/login";
    }
    return this.state.jwttoken !== "" ? (
      <div>
        <div style={{ display: "block", alignItems: "center" }}>
          <button
            type="button"
            className="btn btn-dark"
            style={{ float: "right" }}
            onClick={this.logout}
          >
            LogOut
          </button>
          <button
            type="button"
            className="btn btn-dark"
            onClick={() => this.addAccount()}
          >
            ADD Accounts
          </button>
        </div>
        <div>
          {accounts.map((a, index) => {
            return (
              <div className="card mt-2" key={index}>
                <div className="card-body ">
                  <h5 className="card-title">
                    {a.name}
                    <button
                      type="button"
                      className="btn btn-success"
                      style={{ float: "right" }}
                      onClick={() => this.EditAccount(a)}
                    >
                      Edit Account
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      style={{ float: "right", marginRight: "5px" }}
                      onClick={() => this.DeleteAccount(a._id)}
                    >
                      Remove Account
                    </button>
                  </h5>
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => this.getTransaction(a._id)}
                  >
                    Show Transactions
                  </button>
                  <button
                    type="button"
                    className="btn btn-warning ml-2"
                    onClick={() => this.AddTransaction(a._id)}
                  >
                    ADD Transaction
                  </button>
                </div>
              </div>
            );
          })}
        </div>
        {DeleteMessage ? (
          <div className="text-danger">{DeleteMessage}</div>
        ) : null}
        {showTransaction ? (
          transactions ? (
            transactions.map((t, index) => {
              return (
                <li
                  className="list-group-item list-group-item-primary m-4"
                  key={index}
                >
                  {t.type} - {t.amount}
                  <button
                    type="button"
                    className="btn btn-success"
                    style={{ float: "right" }}
                    onClick={() => this.EditTransaction(t)}
                  >
                    Edit Transaction
                  </button>
                  <button
                    type="button"
                    className="btn btn-danger"
                    style={{ float: "right", marginRight: "5px" }}
                    onClick={() => this.DeleteTransaction(t._id)}
                  >
                    Remove Transaction
                  </button>
                </li>
              );
            })
          ) : (
            <p>No Transactions Available for this Account!!</p>
          )
        ) : null}
        {showAccountForm ? (
          <div style={{ margin: "16px" }}>
            <h1>
              ----{AccountType === "add" ? "ADD Account Here" : "Edit Acoount Here"}
              ----
            </h1>
            <form onSubmit={(e) => this.createAccount(e)}>
              <div className="form-group">
                <label htmlFor="name">Account Name</label>
                <input
                  type="text"
                  className="form-control"
                  aria-describedby="nameHelp"
                  id="name"
                  onChange={this.setValue}
                  defaultValue={accountName ? accountName : ""}
                  required
                />
                <small id="nameHelp" className="form-text text-muted">
                  The Account Name Should be at least 3 character!
                </small>
              </div>
              <button type="submit" className="btn btn-primary">
                {AccountType === "add" ? "Create Account" : "Edit Acoount"}
              </button>
            </form>
            {this.state.successMessage ? (
              <div
                style={{ marginRight: "20px" }}
                className="form-text text-success"
              >
                {this.state.successMessage}
              </div>
            ) : null}
          </div>
        ) : null}
        {showTransactionForm ? (
          <div style={{ margin: "16px" }}>
            <h1>
              ----
              {transactionType === "add"
                ? "ADD Transaction Here"
                : "Edit Transaction Here"}
              ----
            </h1>
            <form onSubmit={(e) => this.createTransaction(e)}>
              <div className="form-group">
                <label htmlFor="type">Type</label>
                <select id="type" class="form-control" onClick={this.setValue} defaultValue="income">
                  <option id="income">Income</option>
                  <option id="expense">Expense</option>
                </select>
                <label htmlFor="amount">Amount</label>
                <input
                  type="Number"
                  min = "0"
                  className="form-control"
                  aria-describedby="nameHelp"
                  id="amount"
                  onChange={this.setValue}
                  defaultValue={amount ? amount : 0}
                  required
                />
                <small id="nameHelp" className="form-text text-muted">
                  The Amount is not to be 0(Zero)!.
                </small>
              </div>
              <button type="submit" className="btn btn-primary">
                {transactionType === "add" ? "ADD Transaction" : "Edit Transaction"}
              </button>
            </form>            
          </div>
        ) : null}
      </div>
    ) : (
      <div>You're not Authorized!!!</div>
    );
  }
}

export default AccountList;
