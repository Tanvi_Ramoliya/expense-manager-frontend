import React, { Component } from "react";
import { Link , Redirect} from "react-router-dom";

class Signup extends Component {
  state = {
      email : "",
      password : '',
      confirmPassword  :'',
      message : "",
      successMessage : "",
      status : undefined
  };
  setValue = (e) => {
    const name = e.target.id;
    const value = e.target.value;
    this.setState({[name] : value});        
  }  
  submit = (event) => { 
    const { email,password , confirmPassword } = this.state;    
    if((password && confirmPassword) !== '' ){ 
      if(password !== confirmPassword){
        this.setState({message : "Password and Confirm Password doesn't match!"});
        event.preventDefault(); 
      }
      else{
        console.log('In fetch',email,password)
        event.preventDefault();
        fetch('http://localhost:3001/signup',{
          method : 'PUT',
          headers : {
            'Content-Type' : 'application/json'
          },
          body : JSON.stringify({ email : email , password : password})
        }).then(result => {
          this.setState({ status : result.status})
          return result.json();
        }).then(r =>{
          this.setState({successMessage : r.message});         
        })
        .catch(err => {
          console.log(err);
        })
      }
  }
   
  }
  render() {
    if (this.state.status === 200) {
      window.location.href = '/login';
    }
    return (
      <div>
        <form onSubmit={e => this.submit(e)} >
          <div className="form-group">
            <label htmlFor="email">Email address</label>
            <input
              type="email"
              className="form-control"
              id="email"
              aria-describedby="emailHelp"
              value={this.state.email}
              onChange = {this.setValue}
              required
            />
            <small id="emailHelp" className="form-text text-muted">
             Don't try with your existing email ID.
            </small>
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              id="password"
              onChange = {this.setValue}
              minLength = "8"
              required
            />
          </div>
          <div className="form-group">
            <label htm="confirmPassword">Confirm Password</label>
            <input
              type="password"
              className="form-control"
              id="confirmPassword"
              onChange = {this.setValue}
              minLength = "8"
              required
            />
            <small id="emailHelp" className="form-text text-danger">
              {this.state.message}
            </small>
          </div>
          <button type="submit" className="btn btn-primary">
            Signup
          </button>
        </form>
        <div style={{ margin: "16px", textAlign: "center" }}>
          Already Registered?
          <Link to="/login">Login Here
          </Link>
        </div>
        {
          this.state.successMessage ? <div style={{marginRight: "20px" }} className="form-text text-success">{this.state.successMessage}</div> : null
        }
      </div>
    );
  }
}

export default Signup;
