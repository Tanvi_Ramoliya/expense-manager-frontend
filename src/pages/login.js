import React, { Component } from "react";
import { Link } from "react-router-dom";

class Login extends Component {
  state = {
    email: "",
    password: "",
    message: "",
    successMessage: "",
    status: undefined,
  };
  componentDidMount = () => {
    localStorage.setItem('jwttoken','');
  }
  setValue = (e) => {
    const name = e.target.id;
    const value = e.target.value;
    this.setState({ [name]: value });
  };
  login = (event) => {
    event.preventDefault();
    const { email, password } = this.state;
    fetch("http://localhost:3001/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      redirect: "follow",
      body: JSON.stringify({ email: email, password: password }),
    })
      .then((result) => {
        this.setState({ status: result.status });
        return result.json();
      })
      .then((r) => {
        this.setState({ successMessage: r.message });
        localStorage.setItem('jwttoken',r.token);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  render() {
    if (this.state.status === 200) {
      window.location.href = '/accounts';
    }
    return (
      <div>
        <form onSubmit={(e) => this.login(e)}>
          <div className="form-group">
            <label htmlFor="email">Email address</label>
            <input
              type="email"
              className="form-control"
              aria-describedby="emailHelp"
              id="email"
              onChange={this.setValue}
              required
            />
            <small id="emailHelp" className="form-text text-muted">
              We'll never share your email with anyone else.
            </small>
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              id="password"
              minLength="8"
              onChange={this.setValue}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </form>
        <div style={{ margin: "16px", textAlign: "center" }}>
          Not Registered Yet?
          <Link to="/signup">signup Here</Link>
        </div>
        {this.state.successMessage ? (
          <div
            style={{ marginRight: "20px" }}
            className="form-text text-success"
          >
            {this.state.successMessage}
          </div>
        ) : null}
      </div>
    );
  }
}

export default Login;
