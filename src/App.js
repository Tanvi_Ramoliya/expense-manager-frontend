import { BrowserRouter, Switch,Link,Route, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './global.css';

import signup from './pages/signup';
import login from './pages/login';
import AccountList from './pages/accounts';

function App() {
  return (
    <div className="app">
      <h1 style={{margin : "10px",textAlign : "center"}}>----Your Expense Manager----</h1>
    <BrowserRouter>
      <Switch>
        <Route path="/" component={login} exact/>
        <Route path="/signup" component={signup} exact/>
        <Route path="/login" component={login} exact/>
        <Route path="/accounts" component={AccountList} exact/>
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
    </div>
  );
}

export default App;
